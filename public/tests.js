describe("Tron", function() {

    describe("findAvailableColors", function() {

        it("suggests all colours if none are taken", function() {
            // Given no players
            Smolpxl.removePlayer(1);
            Smolpxl.removePlayer(0);
            expect(Smolpxl.players()).toEqual([]);

            // All colours are available
            expect(
                findAvailableColors(null, HUMAN_COLORS)
            ).toEqual(HUMAN_COLORS);
        });

        it("suggests red if green is taken", function() {
            // Given 1 red player
            Smolpxl.removePlayer(1);
            Smolpxl.removePlayer(0);
            expect(Smolpxl.players()).toEqual([]);
            Smolpxl.addPlayer({ name: "a", color: HUMAN_COLORS[0].splice() });

            // All colours except red are available
            const expected_colors = HUMAN_COLORS.slice(1);
            expect(expected_colors.length).toBe(HUMAN_COLORS.length - 1);
            expect(
                findAvailableColors(null, HUMAN_COLORS)
            ).toEqual(expected_colors);
        });
    });
});
