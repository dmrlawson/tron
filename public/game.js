"use strict";

const WIDTH = 100;
const HEIGHT = 100;
const BG_COLOR = [25, 25, 25];
const BORDER_COLOR = [202, 254, 242];
const WALL_COLOR = [169, 169, 169];
const START_DELAY_FRAMES = 30;

const HUMAN_COLORS = [
    [255,   0,   0],
    [  0, 203,   0],
    [ 65, 102, 255],
    [202, 202,   0],
    [207,  78, 208],
    [  0, 169, 178],
    [255, 157,  16],
    [227, 140, 227]
];

const BOT_COLORS = [
    [115,  81, 115],
    [145, 118,  78],
    [ 47, 104, 107],
    [134,  90,   0],
    [148, 148,  88],
    [ 82,  91, 129],
    [ 72, 110,  72],
    [151,  88,  88]
];

const START_POSITIONS = [
    {x: 25, y: 25, dir: Smolpxl.directions.DOWN},
    {x: 75, y: 75, dir: Smolpxl.directions.UP},
    {x: 75, y: 25, dir: Smolpxl.directions.LEFT},
    {x: 25, y: 75, dir: Smolpxl.directions.RIGHT},
    {x: 50, y: 25, dir: Smolpxl.directions.DOWN},
    {x: 75, y: 50, dir: Smolpxl.directions.LEFT},
    {x: 25, y: 50, dir: Smolpxl.directions.RIGHT},
    {x: 50, y: 75, dir: Smolpxl.directions.UP},
    {x: 37, y: 30, dir: Smolpxl.directions.UP},
    {x: 63, y: 30, dir: Smolpxl.directions.UP},
    {x: 37, y: 60, dir: Smolpxl.directions.DOWN},
    {x: 63, y: 60, dir: Smolpxl.directions.DOWN},
    {x: 30, y: 37, dir: Smolpxl.directions.LEFT},
    {x: 30, y: 63, dir: Smolpxl.directions.LEFT},
    {x: 60, y: 37, dir: Smolpxl.directions.RIGHT},
    {x: 60, y: 63, dir: Smolpxl.directions.RIGHT}
];

const game = new Smolpxl.Game();

// Player types
const HUMAN = "human";
const BOT = "bot";

// View

function view(screen, model) {
    const readyScreen = model.game.startDelay > 0;

    for (let x = 0; x < WIDTH; x++) {
        for (let y = 0; y < HEIGHT; y++) {
            const playerNum = model.game.grid[(y * WIDTH) + x];
            screen.set(x, y, gridColor(model, playerNum, x, y));
        }
    }
    for (let player of model.game.activePlayers) {
        screen.set(
            player.x,
            player.y,
            player.dead ? player.color : brighten(player.color)
        );
    }

    if (readyScreen) {
        screen.message(["", "", "Ready!", "", ""]);
    }

    const lpn = leadingPlayerName(model);
    const wpn = winningPlayerName(model);
    if (wpn) {
        screen.message(["", "", `${wpn} wins!`, `Leader: ${lpn}`, "", ""])
    }

    screen.messageTopMiddle(
        Smolpxl.players().map(
            player => ({
                text: `${player.name.substring(0, 8)}:${player.score}`,
                color: player.color
            })
        )
    );
}

function winningPlayerName(model) {
    function playerAlive(player) {
        return !player.dead;
    }
    const left = model.game.activePlayers.filter(playerAlive);
    if (left.length > 1 ) {
        return null;  // Should not happen: game is not over if 2 left
    } else if (left.length === 1) {
        return left[0].name;
    } else {
        return "No-one";
    }
}

function leadingPlayerName(model) {
    let max = -1;
    let ret = "No-one";
    for (let player of Smolpxl.players()) {
        if (player.score > max) {
            ret = player.name;
            max = player.score;
        }
    }
    return ret;
}

function gridColor(model, playerNum, x, y) {
    if (playerNum > 0) {
        const col = model.game.activePlayers[playerNum - 1].color;
        return col;
    } else if (x === 0 || y === 0 || x == WIDTH - 1 || y == HEIGHT - 1) {
        return WALL_COLOR;
    } else {
        return BG_COLOR;
    }
}

function brighten(color) {
    return color.map((v) => v + ((255 - v) / 2));
}


// Update

function update(runningGame, model) {
    if (model.game.startDelay > 0) {
        model.game.startDelay--;
    } else if (gameOver(model)) {
        if (
            runningGame.receivedInput("SELECT") ||
            runningGame.receivedInput("LEFT_CLICK")
        ) {
            resetModel(model);
        }
    } else {
        let n = 0;
        for (const player of model.game.activePlayers) {
            if (!player.dead) {
                updatePlayer(runningGame, model, player, n);
            }
            n++;
        }

        for (let i = 0; i < model.game.activePlayers.length; i++) {
            const player = model.game.activePlayers[i];
            if (!player.dead) {
                model.game.grid[gridPos(player.x, player.y)] = i + 1;
            }
        }

        if (gameOver(model)) {
            for (let n = 0; n < model.game.activePlayers.length; n++) {
                if (!model.game.activePlayers[n].dead) {
                    Smolpxl.players()[n].score++;
                }
            }
            Smolpxl.saveSettingsIfEnabled();
        }
    }

    return model;
}

function gameOver(model) {
    return model.game.activePlayers.filter(playerAlive).length <= 1;
}

function playerAlive(player) {
    return !player.dead;
}

class FakeRunningGame {
    receivedInput() {
        return false;
    }
}

function findAvailableColors(model, colorsList) {
    let cols = colorsList.slice();
    for (const player of Smolpxl.players()) {
        if (player.color) {
            let i = cols.find(c => Smolpxl.equalArrays(c, player.color));
            if (i !== -1) {
                cols.splice(i, 1);
                if (cols.length === 0) {
                    // If we ran out, start again
                    cols = colorsList.slice()
                }
            }
        }
    }
    return cols;
}

function completePlayerInfo(model) {
    let availableHumanColors = findAvailableColors(model, HUMAN_COLORS);
    let availableBotColors = findAvailableColors(model, BOT_COLORS);
    let playersUpdated = false;
    for (const player of Smolpxl.players()) {
        if (!player.color) {
            if (player.type === HUMAN) {
                player.color = availableHumanColors.shift();
                if (!player.color) {
                    player.color = HUMAN_COLORS[0];
                    playersUpdated = true;
                }
            } else {
                player.color = availableBotColors.shift();
                if (!player.color) {
                    player.color = BOT_COLORS[0];
                    playersUpdated = true;
                }
            }
        }

        if (player.score === undefined) {
            player.score = 0;
            playersUpdated = true;
        }
    }

    if (playersUpdated) {
        Smolpxl.saveSettingsIfEnabled();
    }
}

function createActivePlayers(model) {
    let numPositions = Math.ceil(Smolpxl.players().length / 4) * 4;
    let positions = START_POSITIONS.slice(0, numPositions);

    model.game.activePlayers = [];
    for (const player of Smolpxl.players()) {
        // Pick a random pos from the list and remove it
        let posI = Smolpxl.randomInt(0, positions.length - 1);
        let pos = positions[posI];
        positions.splice(posI, 1);

        model.game.activePlayers.push(
            {
                x: pos.x,
                y: pos.y,
                dir: pos.dir,
                color: player.color,
                name: player.name,
                type: player.type,
                dead: false
            }
        );
    }
}

function zeroScores(model) {
    for (const player of Smolpxl.players()) {
        player.score = 0;
    }
    return Smolpxl.saveSettings();
}

function resetModel(model) {
    completePlayerInfo(model);
    createActivePlayers(model);

    model.game.grid.fill(0);

    // Step forward a few time steps so you can see which way you're facing
    model.game.startDelay = 0;
    for (let i = 0; i < 3; i++) {
        update(new FakeRunningGame(), model);
    }

    model.game.startDelay = START_DELAY_FRAMES;
}

function gridPos(x, y) {
    return (y * WIDTH) + x;
}

function updatePlayer(runningGame, model, player, playerNumber) {
    if (player.type === HUMAN) {
        if (runningGame.receivedInput("UP", playerNumber)) {
            updatePlayerDirection(player, Smolpxl.directions.UP);
        } else if (runningGame.receivedInput("DOWN", playerNumber)) {
            updatePlayerDirection(player, Smolpxl.directions.DOWN);
        } else if (runningGame.receivedInput("LEFT", playerNumber)) {
            updatePlayerDirection(player, Smolpxl.directions.LEFT);
        } else if (runningGame.receivedInput("RIGHT", playerNumber)) {
            updatePlayerDirection(player, Smolpxl.directions.RIGHT);
        }
    } else if (player.type === BOT) {
        updateCpuPlayer(model, player);
    }

    player.x += player.dir[0];
    player.y += player.dir[1];

    if (
        player.x <= 0 ||
        player.x >= WIDTH - 1 ||
        player.y <= 0 ||
        player.y >= WIDTH - 1 ||
        model.game.grid[gridPos(player.x, player.y)] !== 0
    ) {
        player.dead = true;
        return;
    }
}

function updateCpuPlayer(model, player) {
    // The closer we are to a wall, the more likely
    // we are to turn.
    const dist = distanceToWall(model, player);
    if (dist >= 0) {
        const p = Math.random();
        const relDist = dist / 10.0;
        if (p > relDist) {
            turn90(model, player);
        }
    }

    // Small chance of turning at random
    const p = Math.random();
    if (p < 0.01) {
        turn90(model, player);
    }
}

function distanceToWall(model, player) {
    for (let i = 1; i < 12; i++) {
        if (colorInFront(model, player, i) !== 0) {
            return i-1;
        }
    }
    return -1;
}



function turn90(model, player) {
    const p = Math.random();
    if (p > 0.5) {
        const turned = turnLeft(model, player);
        if (!turned) {
            turnRight(model, player);
        }
    } else {
        const turned = turnRight(model, player);
        if (!turned) {
            turnLeft(model, player);
        }
    }
}

function turnLeft(model, player) {
    const oldDir = player.dir;
    player.dir = turnDirectionLeft(player.dir);
    const ok = colorInFront(model, player, 1) === 0;
    if (!ok) {
        player.dir = oldDir;
    }
    return ok;
}

function turnRight(model, player) {
    const oldDir = player.dir;
    player.dir = turnDirectionRight(player.dir);
    const ok = colorInFront(model, player, 1) === 0;
    if (!ok) {
        player.dir = oldDir;
    }
    return ok;
}

/**
 * Probably should be in Smolpxl
 */
function turnDirectionLeft(dir) {
    switch (dir) {
        case Smolpxl.directions.UP: return Smolpxl.directions.LEFT;
        case Smolpxl.directions.LEFT: return Smolpxl.directions.DOWN;
        case Smolpxl.directions.DOWN: return Smolpxl.directions.RIGHT;
        case Smolpxl.directions.RIGHT: return Smolpxl.directions.UP;
    }
}

/**
 * Probably should be in Smolpxl
 */
function turnDirectionRight(dir) {
    switch (dir) {
        case Smolpxl.directions.UP: return Smolpxl.directions.RIGHT;
        case Smolpxl.directions.RIGHT: return Smolpxl.directions.DOWN;
        case Smolpxl.directions.DOWN: return Smolpxl.directions.LEFT;
        case Smolpxl.directions.LEFT: return Smolpxl.directions.UP;
    }
}

function colorInFront(model, player, distance) {
    const pos = [player.x, player.y];
    const newPos = Smolpxl.coordMoved(pos, player.dir, distance);
    if (
        newPos[0] < 0 ||
        newPos[1] < 0 ||
        newPos[0] >= WIDTH ||
        newPos[1] >= HEIGHT
    ) {
        return 255;
    } else {
        return model.game.grid[gridPos(newPos[0], newPos[1])];
    }
}

function updatePlayerDirection(player, dir) {
    if (!Smolpxl.areOpposite(player.dir, dir)) {
        player.dir = dir;
    }
}


// Model

function startModel() {
    const gridBuffer = new ArrayBuffer(WIDTH * HEIGHT);
    const grid = new Uint8Array(gridBuffer);

    const ret = {
        game: {
            activePlayers: [],
            startDelay: 30,
            grid
        }
    };
    resetModel(ret);
    return ret;
}

function titleMessage(model) {
    return [
        "",
        "Smolpxl Tron",
        "",
        "<SELECT> to start",
        "",
        "<MENU> to set up players",
        describePlayers(model),
        ""
    ];
}

function describePlayers(model) {
    let humans = 0;
    let bots = 0;
    for (let player of Smolpxl.players()) {
        switch (player.type) {
            case HUMAN:
                humans++;
                break;
            case BOT:
                bots++;
                break;
        }
    }
    const h = humans === 1 ? "human" : "humans";
    const b = bots === 1 ? "bot" : "bots";
    return `(${humans} ${h}, ${bots} ${b})`;
}

class FakeEvent {
    preventDefault() {}
}

function newPlayerName(model, prefix) {
    let n = 1;
    let ret = prefix + n;
    while (Smolpxl.players().some((p) => p.name === ret)) {
        n++;
        ret = prefix + n;
    }
    return ret;
}

function playerMenuItems(model, refreshMenu) {
    function addHuman(menu) {
        const players = Smolpxl.players();
        if (players.length >= START_POSITIONS.length) {
            return;
        }
        let firstNonHuman = players.findIndex(p => p.type !== HUMAN);
        if (firstNonHuman === -1) {
            firstNonHuman = players.length;
        }

        const newPlayer = {name: newPlayerName(model, "Human "), type: HUMAN};
        Smolpxl.addPlayer(newPlayer, firstNonHuman).then(() => {
            resetModel(model);
            refreshMenu();
            menu.render();
            menu.moveDown(new FakeEvent());
        });
    }

    function addBot(menu) {
        if (Smolpxl.players().length >= START_POSITIONS.length) {
            return;
        }
        Smolpxl.addPlayer(
            {name: newPlayerName(model, "Bot "), type: BOT}
        ).then(() => {
            resetModel(model);
            refreshMenu();
            menu.render();
            menu.moveDown(new FakeEvent());
        });
    }

    function renamePlayerFn(n, player) {
        return function(menu) {
            Smolpxl.renamePlayer(n, player).then((newName) => {
                if (newName) {
                    model.game.activePlayers[n].name = newName;
                    refreshMenu();
                    menu.render();
                }
            });
        };
    }

    function deletePlayerFn(n) {
        return function(menu) {
            Smolpxl.removePlayer(n).then(() => {
                resetModel(model);
                menu.moveOut();
                refreshMenu();
                menu.render();
            });
        };
    }

    function controlsMenuItems(player, n) {
        if (player.type === HUMAN) {
            return [
                {
                    "text": "Controls: " + Smolpxl.summariseControls(n),
                    "color": player.color,
                    "fn": (menu) => Smolpxl.changeControls(
                        menu, player, n, refreshMenu)
                }
            ];
        } else {
            return [];
        }
    }

    let ret = [];

    ret.push({"text": "players"});
    ret.push({"text": "back", "fn": (menu) => menu.moveOut()});

    let n = 0;
    for (let player of Smolpxl.players()) {
        const controlsItems = controlsMenuItems(player, n);

        ret.push(
            {
                "text": `Edit ${player.name}`,
                "color": player.color,
                "items": [
                    {
                        "text": `edit ${player.name}`.toLowerCase()
                    },
                    {
                        "text": "back",
                        "fn": (menu) => menu.moveOut()
                    },
                    {
                        "text": "Change name",
                        "color": player.color,
                        "fn": renamePlayerFn(n, player)
                    },
                    ...controlsItems,
                    {
                        "text": `Delete ${player.name}`,
                        "color": player.color,
                        "fn": deletePlayerFn(n)
                    }
                ]
            }
        );
        n++;
    }
    ret.push({"text": "Add Human", "fn": addHuman});
    ret.push({"text": "Add Bot", "fn": addBot});
    return ret;
}

function refreshMenuFn(game, model) {
    return function() {
        function touchControlsText() {
            if (game.touchControlsVisible()) {
                return "Hide touch controls";
            } else {
                return "Show touch controls";
            }
        }

        const pl = playerMenuItems(model, refreshMenuFn(game, model));
        game.setMenu(
            [
                {"text": "paused"},
                {"text": "continue", "fn": (menu) => menu.off()},
                {
                    "text": "Reset scores",
                    "fn": (menu) => {
                        resetModel(model);
                        zeroScores(model).then(() => {
                            game.endGame();
                            menu.off();
                        });
                    }
                },
                {"text": "Players", "items": pl},
                {
                    "text": "Settings",
                    "items": [
                        { "text": "back", "fn": menu => menu.moveOut() },
                        Smolpxl.saveSettingsMenuItem(
                            refreshMenuFn(game, model)),
                        {
                            "text": touchControlsText(),
                            "fn": (menu) => {
                                game.toggleTouchControls();
                                refreshMenuFn(game, model)();
                                menu.render();
                            }
                        }
                    ]
                }
            ]
        );
        game.setTitleMessage(titleMessage(model));
    };
}

(function() {
    Smolpxl.setDefaultPlayers(
        [
            {name: "Human 1", type: HUMAN},
            {name: "Bot 1", type: BOT}
        ]
    );

    const model = startModel();

    game.sendPopularityStats();
    game.showSmolpxlBar();
    game.setPlayerInputs(["UP", "DOWN", "LEFT", "RIGHT"]);
    game.setSourceCodeUrl(
        "https://gitlab.com/smolpxl/tron"
    );
    game.setSize(WIDTH, HEIGHT);
    game.setFps(20);
    game.setBorderColor(BORDER_COLOR);
    game.setBackgroundColor(BG_COLOR);
    game.setTitle("Smolpxl Tron");

    refreshMenuFn(game, model)();
    game.addHook(
        Smolpxl.HOOK_TOUCH_CONTROLS_TOGGLE,
        () => {
            refreshMenuFn(game, model)();
            game._.menu.render();
        }
    );

    game.setTitleMessage(
        titleMessage(model)
    );

    game.start("tron", model, view, update);
}());
