+ Handle a player being deleted during a game
+ Give players editable names and use them in menus
+ Detect someone won and announce it
+ Restart game after won screen
+ Display players in their colour in the menu
+ Keep existing player colours even when we add/delete other players
+ Track scores and display at top
+ Colours for scores at top
+ Continue playing by default when game ends - "end game" menu option to stop
+ Allow resetting scores
+ Allow menus to have titles
+ Allow customising controls
+ Fix similar bot colours
+ Display overall leader on winning screen
+ Fix name generation to avoid clashes
+ Display on title screen how many players there are
+ Rewrite players/key mappings structure
+ Store players definition in Smolpxl
+ Save existing config
+ Provide Add/Remove touch controls options
- Save config (touch controls visibility)

- Release the game!

- Move global stuff into Game
- Put smolpxl (js and css) changes upstream
- Check all TODOs in code

- Remote play - hardcode remote people are players 2, 3 etc.
- Remote play including providing names, remapping controls
- Remote play UI shows connected players with names
- Remote play allows reconnecting clients
- Remote play allows reconnecting server

- Smolpxl: move globals into Game
- Smolpxl: decide on Promises or async/await and follow through
