# Tron

Tron is a simple game of controlling growing lines.  Play at
[smolpxl.gitlab.io/tron](https://smolpxl.gitlab.io/tron/).

![](tron.gif)

This version allows humans and bots to compete.

Play this and other games at
[smolpxl.artificialworlds.net](https://smolpxl.artificialworlds.net).

Tron is written using the
[Smolpxl library](https://gitlab.com/smolpxl/smolpxl).

## Updating smolpxl.js

We store a copy of smolpxl.js in the source code.  To update it, modify
[Makefile](Makefile), changing the value of `SMOLPXL_JS_VERSION` to specify the
version you want, and run:

(Make sure you have GNU Make and `curl` first.)

```sh
make download-smolpxl-js
```

## License and credits

Copyright 2021 Andy Balaam and contributors, released under the
[AGPLv3 license](LICENSE) or later.

Contains icons from the
[Feather Icons](https://github.com/feathericons/feather) set, which is
Copyright 2013-2017 Cole Bemis, and released under the
[MIT License](https://github.com/feathericons/feather/blob/8b5d6802fa8fd1eb3924b465ff718d2fa8d61efe/LICENSE).

Contains a copy of Jasmine 3.6.0 unit testing library, which is Copyright
2008-2019 Pivotal Labs, and released under the
[MIT license](tests/MIT.LICENSE).

Uses [shareon](https://shareon.js.org/) by Nikita Karamov to provide the
social sharing buttons.  (The code is dynamically loaded when the Share button
is clicked.)

## Code of conduct

Please note that this project is released with a
[Contributor Code of Conduct](code_of_conduct.md).  By participating in this
project you agree to abide by its terms.

[![Contributor Covenant](images-src/contributor-covenant-v2.0-adopted-ff69b4.svg)](code_of_conduct.md)
